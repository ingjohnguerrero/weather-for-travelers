//
//  OpenWeatherMap.h
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/3/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol OpenWeatherMapDelegate <NSObject>

@required
- (void) currentLocationReceived:(NSDictionary *)responseDict;
- (void) locationDataReceived:(NSDictionary *)responseDict;
@end

@interface OpenWeatherMap : NSObject{
    id <OpenWeatherMapDelegate> _delegate;
}

@property id delegate;

- (void)getLocalWeatherDataWithLocation:(CLLocation *)weatherLocation;

- (void)getWeatherDataWithLocation:(CLLocation *)weatherLocation;

- (void)getWeatherDataWithLocationArray:(NSArray *)weatherLocationsArray;

+ (NSString *)getWeatherImagePathWithIcon:(NSString *)iconName;

@end
