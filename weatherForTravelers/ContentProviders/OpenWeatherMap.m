//
//  OpenWeatherMap.m
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/3/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "OpenWeatherMap.h"

@implementation OpenWeatherMap : NSObject 

static NSString *openWeatherUrl = @"http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=c1e72fd5755094d5823ae5754a336359&units=metric&lang=sp";

static NSString *openWeatherIconUrl = @"http://openweathermap.org/img/w/%@.png";

-(void)getLocalWeatherDataWithLocation:(CLLocation *)weatherLocation{
        
    NSString *locationWeatherUrl = [NSString stringWithFormat:openWeatherUrl, [weatherLocation coordinate].latitude, [weatherLocation coordinate].longitude];
    
    NSURL *URL = [NSURL URLWithString:locationWeatherUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if (!error) {
                                          // TODO 1: More coming here!
                                          NSError *localError = nil;
                                          NSDictionary *dataDictinary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                                          if (!localError) {
                                              NSLog(@"%@", dataDictinary);
                                              [_delegate currentLocationReceived: dataDictinary];
                                          }
                                      }else{
                                          NSLog(@"%@", [error description]);
                                      }
                                  }];
    
    [task resume];
}

- (void)getWeatherDataWithLocationArray:(NSArray *)weatherLocationsArray{
    
    
    for (CLLocation *weatherLocation in weatherLocationsArray) {
        NSString *locationWeatherUrl = [NSString stringWithFormat:openWeatherUrl, [weatherLocation coordinate].latitude, [weatherLocation coordinate].longitude];
        
        
        NSURL *URL = [NSURL URLWithString:locationWeatherUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if (!error) {
                                              // TODO 1: More coming here!
                                              NSError *localError = nil;
                                              NSDictionary *dataDictinary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                                              if (!localError) {
                                                  NSLog(@"Data downloded");
                                                  [_delegate locationDataReceived:dataDictinary];
                                              }
                                          }else{
                                              NSLog(@"%@", [error description]);
                                          }
                                      }];
        
        [task resume];
    }
}

-(void)getWeatherDataWithLocation:(CLLocation *)weatherLocation{
    
    NSString *locationWeatherUrl = [NSString stringWithFormat:openWeatherUrl, [weatherLocation coordinate].latitude, [weatherLocation coordinate].longitude];
    
    NSURL *URL = [NSURL URLWithString:locationWeatherUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if (!error) {
                                          // TODO 1: More coming here!
                                          NSError *localError = nil;
                                          NSDictionary *dataDictinary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
                                          if (!localError) {
                                              NSLog(@"%@", dataDictinary);
                                              [_delegate locationDataReceived: dataDictinary];
                                          }
                                      }else{
                                          NSLog(@"%@", [error description]);
                                      }
                                  }];
    
    [task resume];
}

+ (NSString *)getWeatherImagePathWithIcon:(NSString *)iconName{
    return [NSString stringWithFormat:openWeatherIconUrl, iconName];
}

@end
