//
//  AddLocationViewController.h
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
@import GoogleMaps;

@interface AddLocationViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate>{
}

@property id delegate;
@property CLLocation *selectedLocation;
@property GMSMarker *selectedLocationMarker;
@property GMSAddress* addressObj;

@end
