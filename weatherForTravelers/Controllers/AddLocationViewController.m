//
//  AddLocationViewController.m
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "AddLocationViewController.h"

@implementation AddLocationViewController{
    GMSMapView *mapView_;
    CLLocation *currentLocation;
    GMSMarker *currentLocationMarker;
    CLLocationManager *locationManager;
    NSMutableArray *mapMarkers;
    UIBarButtonItem *rightNavButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // Device location manager
    
    mapMarkers = [NSMutableArray new];
    
    [self locationManagerSetUp];
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManagerSetUp{
    if (locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.activityType = CLActivityTypeFitness;
    
    // Movement threshold for new events.
    locationManager.distanceFilter = 10; // meters
    
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
}

- (void)configureView {
    [self drawGoogleMapView];
    [self showInstructionAlert];
}


- (void)saveSelectedLocation:(id)sender {
    MasterViewController *controller = (MasterViewController *) [self.navigationController.viewControllers objectAtIndex:0];
    
    controller.selectedLocation = _selectedLocation;
    //Location info
    controller.selectedAddressObj = _addressObj;
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (UIBarButtonItem *)rightNavButton{
    if (!rightNavButton) {
        rightNavButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveSelectedLocation:)];
    }
    return rightNavButton;
}

- (void)showInstructionAlert{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Nuevo sitio"
                                          message:@"Navega por el mapa. Cuando escojas tu destino, deja pulsado hasta que aparezca el marcado y luego confirma, pulsando guardar."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Google maps delegate
- (void)drawGoogleMapView{
    // Create a GMSCameraPosition that tells the map to display it self.
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self->currentLocation.coordinate.latitude
                                                            longitude:self->currentLocation.coordinate.longitude
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.delegate = self;
    //mapView_.myLocationEnabled = YES;
    self.view = mapView_;
}

-(void)updateCurrentLocationWithCoordinates:(CLLocation *) newLocation
{
    if (currentLocationMarker == nil) {
        currentLocationMarker = [GMSMarker markerWithPosition:[newLocation coordinate]];
        currentLocationMarker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
        currentLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
        currentLocationMarker.title = @"Aqui estas";
        currentLocationMarker.snippet = @"Home";
        currentLocationMarker.map = mapView_;
    }else{
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        currentLocationMarker.position = [newLocation coordinate];
        [CATransaction commit];
    }
    
}

-(void)updateSelectedLocationWithCoordinates:(CLLocation *) newLocation
{
    if (_selectedLocationMarker == nil) {
        _selectedLocationMarker = [GMSMarker markerWithPosition:[newLocation coordinate]];
        _selectedLocationMarker.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
        _selectedLocationMarker.appearAnimation = kGMSMarkerAnimationPop;
        _selectedLocationMarker.title = @"Nuevo clima";
        _selectedLocationMarker.snippet = @"Localizando";
        _selectedLocationMarker.map = mapView_;
    }else{
        [CATransaction begin];
        [CATransaction setAnimationDuration:1.0];
        _selectedLocationMarker.position = [newLocation coordinate];
        [CATransaction commit];
    }
    [self updateSelectedLocationReverseGeocoding:[newLocation coordinate]];
}

-(void)updateSelectedLocationReverseGeocoding:(CLLocationCoordinate2D) selectedLocationCoordinate{
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:selectedLocationCoordinate completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
        NSLog(@"reverse geocoding results:");
        
        _addressObj = [[response results]objectAtIndex:0];
        NSLog(@"coordinate.latitude=%f", _addressObj.coordinate.latitude);
        NSLog(@"coordinate.longitude=%f", _addressObj.coordinate.longitude);
        NSLog(@"thoroughfare=%@", _addressObj.thoroughfare);
        NSLog(@"locality=%@", _addressObj.locality);
        NSLog(@"subLocality=%@", _addressObj.subLocality);
        NSLog(@"administrativeArea=%@", _addressObj.administrativeArea);
        NSLog(@"postalCode=%@", _addressObj.postalCode);
        NSLog(@"country=%@", _addressObj.country);
        NSLog(@"lines=%@", _addressObj.lines);
        
        _selectedLocationMarker.title = _addressObj.locality;
        _selectedLocationMarker.snippet = _addressObj.country;
        [mapView_ setSelectedMarker:_selectedLocationMarker];
        /*
        for(GMSAddress* addressObj in [response results])
        {
            NSLog(@"coordinate.latitude=%f", addressObj.coordinate.latitude);
            NSLog(@"coordinate.longitude=%f", addressObj.coordinate.longitude);
            NSLog(@"thoroughfare=%@", addressObj.thoroughfare);
            NSLog(@"locality=%@", addressObj.locality);
            NSLog(@"subLocality=%@", addressObj.subLocality);
            NSLog(@"administrativeArea=%@", addressObj.administrativeArea);
            NSLog(@"postalCode=%@", addressObj.postalCode);
            NSLog(@"country=%@", addressObj.country);
            NSLog(@"lines=%@", addressObj.lines);
            
            _selectedLocationMarker.title = addressObj.locality;
            _selectedLocationMarker.snippet = addressObj.country;
            [mapView_ setSelectedMarker:_selectedLocationMarker];
        }*/
    }];
}

-(void) mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    NSLog(@"User's tap latitude is: %f", coordinate.latitude );
    NSLog(@"User's tap longitude is: %f", coordinate.longitude );
}

-(void) mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    _selectedLocation = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    NSLog(@"User's long press latitude is: %f", coordinate.latitude );
    NSLog(@"User's long press longitude is: %f", coordinate.longitude );
    
    self.navigationItem.rightBarButtonItem = self.rightNavButton;
    
    [self updateSelectedLocationWithCoordinates:_selectedLocation];
}

#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status == kCLAuthorizationStatusAuthorizedAlways){
        
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:@"No fue posible ubicarte, revisa los permisos de la app"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    self->currentLocation = newLocation;
    
    [self updateCurrentLocationWithCoordinates: self->currentLocation];
    
    GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget: [currentLocation coordinate]];
    
    [mapView_ moveCamera:cameraUpdate];
}

@end
