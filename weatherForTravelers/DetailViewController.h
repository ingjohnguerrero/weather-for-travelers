//
//  DetailViewController.h
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Forecast.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UILabel *tempLabel;
@property (weak, nonatomic) IBOutlet UIImageView *weatheImage;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescription;
@property (weak, nonatomic) IBOutlet UILabel *pressureLbl;
@property (weak, nonatomic) IBOutlet UILabel *hrLbl;
@property (weak, nonatomic) IBOutlet UILabel *tempMinLbl;
@property (weak, nonatomic) IBOutlet UILabel *tempMaxLbl;
@property (weak, nonatomic) IBOutlet UILabel *measureDateTimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *sunriseLbl;
@property (weak, nonatomic) IBOutlet UILabel *sunsetLbl;

@end

