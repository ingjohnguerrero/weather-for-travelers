//
//  DetailViewController.m
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        _locationLbl.text = [NSString stringWithFormat:@"%@, %@", [(Forecast *)self.detailItem locationName], [(Forecast *)self.detailItem country_code]];
        self.title = [(Forecast *)self.detailItem locationName];
        self.tempLabel.text = [NSString stringWithFormat:@"%@ ºC", [(Forecast *)self.detailItem temp]];
        [_weatheImage setImage:[(Forecast *)self.detailItem weatherImage]];
        _weatherDescription.text = [(Forecast *)self.detailItem weather_description];
        _pressureLbl.text = [NSString stringWithFormat:@"%@ hPa",[(Forecast *)self.detailItem pressure] ];
        _hrLbl.text = [NSString stringWithFormat:@"%@ %%", [(Forecast *)self.detailItem humidity] ];
        _tempMinLbl.text = [NSString stringWithFormat:@"%@ ºC", [(Forecast *)self.detailItem temp_min]];
        _tempMaxLbl.text = [NSString stringWithFormat:@"%@ ºC", [(Forecast *)self.detailItem temp_max]];
        
        _measureDateTimeLbl.text = [self getStringFromDate:[(Forecast *)self.detailItem measure_date_time]];
        _sunriseLbl.text = [self getStringFromDate:[(Forecast *)self.detailItem sunrise]];
        _sunsetLbl.text = [self getStringFromDate:[(Forecast *)self.detailItem sunset]];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *) getStringFromDate:(NSDate *)myDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss zzz"];
    
    return [formatter stringFromDate:myDate];
}

@end
