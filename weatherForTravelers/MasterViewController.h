//
//  MasterViewController.h
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "OpenWeatherMap.h"
#import "AddLocationViewController.h"
#import "CurrentLocationViewCell.h"
#import "Forecast.h"
@import GoogleMaps;

@class DetailViewController;

@interface MasterViewController : UITableViewController<CLLocationManagerDelegate, OpenWeatherMapDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;

@property CLLocation * selectedLocation;
@property Forecast *currentLocationForecast;
@property GMSAddress* selectedAddressObj;
@property (strong, nonatomic) CLGeocoder *geocoder;

@end

