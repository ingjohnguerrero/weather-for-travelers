//
//  MasterViewController.m
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"


@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController{
    CLLocation *currentLocation;
    CLLocationManager *locationManager;
    OpenWeatherMap *openWeatherMap;
    NSString *currentLocationName;
    NSString *currentCountry;
    NSMutableArray *locationsArray;
    NSMutableArray *locationsForecastArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0 green:0.57 blue:1 alpha:0.22]];
    
    openWeatherMap = [OpenWeatherMap new];
    openWeatherMap.delegate = self;
    
    _currentLocationForecast = [Forecast new];
    
    _objects = [NSMutableArray new];
    locationsArray = [NSMutableArray new];
    
    self.title = @"Mis sitios";
    
    [self locationManagerSetUp];
}

- (void)locationManagerSetUp{
    if (locationManager == nil) {
        locationManager = [[CLLocationManager alloc] init];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.activityType = CLActivityTypeFitness;
    
    // Movement threshold for new events.
    locationManager.distanceFilter = 10; // meters
    
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
}

- (void)loadLocationArrayData{
    [openWeatherMap getWeatherDataWithLocationArray:locationsForecastArray];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    if(_selectedLocation){
        NSLog(@"%@", _selectedLocation);
        [locationsArray addObject:_selectedLocation];
        [openWeatherMap getWeatherDataWithLocation:_selectedLocation];
        _selectedLocation = nil;
    }
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    //
    //    [self.objects insertObject:[NSDate date] atIndex:0];
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    //    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.navigationController performSegueWithIdentifier:@"addLocation" sender:self];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Forecast *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }else if ([[segue identifier] isEqualToString:@"addLocation"]){
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CurrentLocationViewCell *cell = (CurrentLocationViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CurrentLocationCell" forIndexPath:indexPath];
    Forecast *locationForecast = [_objects objectAtIndex:indexPath.row];
    cell.locationName.text = [NSString stringWithFormat:@"%@, %@",locationForecast.locationName, locationForecast.country_code];
    cell.tempLbl.text = [NSString stringWithFormat:@"%@ ºC",[locationForecast temp]];
    cell.hrLbl.text = [NSString stringWithFormat:@"%@%% HR", [locationForecast humidity]];
    cell.weatherDescription.text = locationForecast.weather_description;
    UIImage *image = [locationForecast weatherImage];
    [cell.weatherImage setImage:image];
    
    tableView.rowHeight = 130;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

#pragma mark - Core Location Delegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Error"
                                          message:@"No fue posible ubicarte, revisa los permisos de la app"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    [alertController addAction:okAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    if(self->currentLocation == nil){
        [openWeatherMap getLocalWeatherDataWithLocation:newLocation];
    }
    self->currentLocation = newLocation;
}

/*
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // Make sure this is a recent location event
    CLLocation *newLocation = [locations lastObject];
    NSTimeInterval eventInterval = [newLocation.timestamp timeIntervalSinceNow];
    if(fabs(eventInterval) < 30.0)
    {
        // Make sure the event is valid
        if (newLocation.horizontalAccuracy < 0)
        {
            return;
        }
        
        
        // Instantiate _geoCoder if it has not been already
        if (self.geocoder == nil)
        {
            self.geocoder = [[CLGeocoder alloc] init];
        }
        
        
        //Only one geocoding instance per action
        //so stop any previous geocoding actions before starting this one
        if([self.geocoder isGeocoding])
        {
            [self.geocoder cancelGeocode];
        }
        
        
        [self.geocoder reverseGeocodeLocation: newLocation
                            completionHandler: ^(NSArray* placemarks, NSError* error)
         {
             if([placemarks count] > 0)
             {
                 CLPlacemark *foundPlacemark = [placemarks objectAtIndex:0];
                 currentLocationName =
                 [NSString stringWithFormat:@"You are in: %@",
                  foundPlacemark.description];
             }
             else if (error.code == kCLErrorGeocodeCanceled)
             {
                 NSLog(@"Geocoding cancelled");
             }
             else if (error.code == kCLErrorGeocodeFoundNoResult)
             {
                 currentLocationName=@"No geocode result found";
             }
             else if (error.code == kCLErrorGeocodeFoundPartialResult)
             {
                 currentLocationName=@"Partial geocode result";
             }
             else
             {
                 currentLocationName =
                 [NSString stringWithFormat:@"Unknown error: %@",
                  error.description];
             }
         }
         ];
        
    }
}*/

#pragma mark - OpenWeatherMapDelegate
- (void)currentLocationReceived:(NSDictionary *)responseDict{
    NSLog(@"Response by delegate: %@", responseDict);
    _currentLocationForecast = [Forecast instanceFromDictinary:responseDict];
    dispatch_async(dispatch_get_main_queue(), ^{
        // code here
        //[_currentLocationForecast setLocationName:currentLocationName];
        //[_currentLocationForecast setCountry:_selectedAddressObj.country];
        NSString *weatherImagePath = [OpenWeatherMap getWeatherImagePathWithIcon:[_currentLocationForecast weather_icon]];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:weatherImagePath]]];
        [_currentLocationForecast setWeatherImage:image];
        
        [self.objects insertObject:_currentLocationForecast atIndex:0];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        NSLog(@"%@", _currentLocationForecast);
    });
}

- (void)locationDataReceived:(NSDictionary *)responseDict{
    NSLog(@"%@", responseDict);
    Forecast *selectedForecast = [Forecast new];
    selectedForecast = [Forecast instanceFromDictinary:responseDict];
    dispatch_async(dispatch_get_main_queue(), ^{
        // code here
        [selectedForecast setLocationName:_selectedAddressObj.locality];
        [selectedForecast setCountry:_selectedAddressObj.country];
        NSString *weatherImagePath = [OpenWeatherMap getWeatherImagePathWithIcon:[selectedForecast weather_icon]];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:weatherImagePath]]];
        [selectedForecast setWeatherImage:image];
        
        [self.objects insertObject:selectedForecast atIndex:0];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        NSLog(@"%@", selectedForecast);
        [locationsForecastArray addObject:selectedForecast];
        _selectedAddressObj = nil;
    });
}

@end
