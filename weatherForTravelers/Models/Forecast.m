//
//  Forecast.m
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/2/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "Forecast.h"

@implementation Forecast

+(Forecast *)instanceFromDictinary:(NSDictionary *)aDictionary{
    Forecast *instance = [[Forecast alloc] init];
    [instance setAttributesFromDictionary:aDictionary];
    return instance;
}

- (void)setAttributesFromDictionary:(NSDictionary *)aDictionary {
    
    if (![aDictionary isKindOfClass:[NSDictionary class]]) {
        return;
    }
    
    [self setValuesForKeysWithDictionary:aDictionary];
}

-(void)setValue:(id)value forKey:(NSString *)key{
    NSLog(@"key passed: %@", key);
    if ([key  isEqual: @"clouds"]) {
        [super setValue:[value objectForKey:@"all"] forKey:@"cloud_density"];
        NSLog(@"cloud_density");
    }
    else if ([key isEqual:@"coord"]){
        [super setValue:[value objectForKey:@"lat"] forKey:@"latitude"];
        [super setValue:[value objectForKey:@"lon"] forKey:@"longitude"];
        NSLog(@"coord loaded");
    }else if ([key isEqual:@"dt"]){
        NSDate *measureDateTime = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
        [super setValue:measureDateTime forKey:@"measure_date_time"];
        NSLog(@"Measure datetime:%@", measureDateTime);
    }else if ([key isEqual:@"main"]){
        [super setValue:[value objectForKey:@"temp_max"] forKey:@"temp_max"];
        [super setValue:[value objectForKey:@"temp_min"] forKey:@"temp_min"];
        [super setValue:[value objectForKey:@"temp"] forKey:@"temp"];
        [super setValue:[value objectForKey:@"humidity"] forKey:@"humidity"];
        [super setValue:[value objectForKey:@"sea_level"] forKey:@"sea_level"];
        [super setValue:[value objectForKey:@"grnd_level"] forKey:@"grnd_level"];
        [super setValue:[value objectForKey:@"pressure"] forKey:@"pressure"];
        NSLog(@"Main info loaded");
    }else if ([key isEqual:@"name"]){
        [super setValue:value forKey:@"locationName"];
        NSLog(@"Location name loaded");
    }else if ([key isEqual:@"rain"]){
        [super setValue:[value objectForKey:@"3h"] forKey:@"rain"];
        NSLog(@"Rain loaded");
    }else if ([key isEqual:@"snow"]){
        [super setValue:[value objectForKey:@"3h"] forKey:@"snow"];
        NSLog(@"Snow loaded");
    }else if ([key isEqual:@"sys"]){
        [super setValue:[value objectForKey:@"country"] forKey:@"country_code"];
        NSDate *sunriseDateTime = [NSDate dateWithTimeIntervalSince1970:[[value objectForKey:@"sunrise"]doubleValue]];
        NSDate *sunsetDateTime = [NSDate dateWithTimeIntervalSince1970:[[value objectForKey:@"sunset"]doubleValue]];
        [super setValue:sunriseDateTime forKey:@"sunrise"];
        [super setValue:sunsetDateTime forKey:@"sunset"];
        NSLog(@"sunrise:%@", sunriseDateTime);
        NSLog(@"sunset:%@", sunsetDateTime);
    }else if ([key isEqual:@"weather"]){
        id newValue = [value objectAtIndex:0];
        [super setValue:[newValue objectForKey:@"description"] forKey:@"weather_description"];
        [super setValue:[newValue objectForKey:@"icon"] forKey:@"weather_icon"];
        NSLog(@"weather info loaded");
    }else if ([key isEqualToString:@"wind"]){
        [super setValue:[value objectForKey:@"deg"] forKey:@"wind_deg"];
        [super setValue:[value objectForKey:@"speed"] forKey:@"wind_speed"];
        NSLog(@"Wind info loaded");
    }else{
        
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    
    if ([key isEqualToString:@"id"]) {
//        [self setValue:value forKey:@"item_id"];
    }
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_forecast_id forKey:@"forecast_id"];
    [aCoder encodeObject:_weather_description forKey:@"weather_description"];
    [aCoder encodeObject:_weather_icon forKey:@"weather_icon"];
    [aCoder encodeObject:_temp forKey:@"temp"];
    [aCoder encodeObject:_pressure forKey:@"pressure"];
    [aCoder encodeObject:_humidity forKey:@"humidity"];
    [aCoder encodeObject:_temp_min forKey:@"temp_min"];
    [aCoder encodeObject:_temp_max forKey:@"temp_max"];
    [aCoder encodeObject:_sea_level forKey:@"sea_level"];
    [aCoder encodeObject:_grnd_level forKey:@"grnd_level"];
    [aCoder encodeObject:_wind_speed forKey:@"wind_speed"];
    [aCoder encodeObject:_wind_deg forKey:@"wind_deg"];
    [aCoder encodeObject:_cloud_density forKey:@"cloud_density"];
    [aCoder encodeObject:_rain forKey:@"rain"];
    [aCoder encodeObject:_snow forKey:@"snow"];
    [aCoder encodeObject:_measure_date_time forKey:@"measure_date_time"];
    [aCoder encodeObject:_sunrise forKey:@"sunrise"];
    [aCoder encodeObject:_sunset forKey:@"sunset"];
    [aCoder encodeObject:_locationName forKey:@"locationName"];
    [aCoder encodeObject:_country forKey:@"country"];
    [aCoder encodeObject:_country_code forKey:@"country_code"];
    [aCoder encodeObject:_latitude forKey:@"latitude"];
    [aCoder encodeObject:_longitude forKey:@"longitude"];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    _forecast_id = [aDecoder decodeObjectForKey: @"forecast_id"];
    _weather_description = [aDecoder decodeObjectForKey: @"weather_description"];
    _weather_icon = [aDecoder decodeObjectForKey: @"weather_icon"];
    _temp = [aDecoder decodeObjectForKey: @"temp"];
    _pressure = [aDecoder decodeObjectForKey: @"pressure"];
    _humidity = [aDecoder decodeObjectForKey: @"humidity"];
    _temp_min = [aDecoder decodeObjectForKey: @"temp_min"];
    _temp_max = [aDecoder decodeObjectForKey: @"temp_max"];
    _sea_level = [aDecoder decodeObjectForKey: @"sea_level"];
    _grnd_level = [aDecoder decodeObjectForKey: @"grnd_level"];
    _wind_speed = [aDecoder decodeObjectForKey: @"wind_speed"];
    _wind_deg = [aDecoder decodeObjectForKey: @"wind_deg"];
    _cloud_density = [aDecoder decodeObjectForKey: @"cloud_density"];
    _rain = [aDecoder decodeObjectForKey: @"rain"];
    _snow = [aDecoder decodeObjectForKey: @"snow"];
    _measure_date_time = [aDecoder decodeObjectForKey: @"measure_date_time"];
    _sunrise = [aDecoder decodeObjectForKey: @"sunrise"];
    _sunset = [aDecoder decodeObjectForKey: @"sunset"];
    _locationName = [aDecoder decodeObjectForKey: @"locationName"];
    _country = [aDecoder decodeObjectForKey: @"country"];
    _country_code = [aDecoder decodeObjectForKey: @"country_code"];
    _latitude = [aDecoder decodeObjectForKey: @"latitude"];
    _longitude = [aDecoder decodeObjectForKey: @"longitude"];
    return self;
}

+(NSString *)getHeadingByDeg:(float)rotationDeg
{
    NSString *headingText;
    float value = rotationDeg;
    if(value >= 0 && value < 23)
    {
        headingText = [NSString stringWithFormat:@"%f° N",value];
    }
    else if(value >=23 && value < 68)
    {
        headingText = [NSString stringWithFormat:@"%f° NE",value];
    }
    else if(value >=68 && value < 113)
    {
        headingText = [NSString stringWithFormat:@"%f° E",value];
    }
    else if(value >=113 && value < 185)
    {
        headingText = [NSString stringWithFormat:@"%f° SE",value];
    }
    else if(value >=185 && value < 203)
    {
        headingText = [NSString stringWithFormat:@"%f° S",value];
    }
    else if(value >=203 && value < 249)
    {
        headingText = [NSString stringWithFormat:@"%f° SE",value];
    }
    else if(value >=249 && value < 293)
    {
        headingText = [NSString stringWithFormat:@"%f° W",value];
    }
    else if(value >=293 && value < 350)
    {
        headingText = [NSString stringWithFormat:@"%f° NW",value];
    }
    
    return headingText;
}

@end

