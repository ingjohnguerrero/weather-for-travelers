//
//  CurrentLocationViewCell.h
//  weatherForTravelers
//
//  Created by John Edwin Guerrero Ayala on 2/3/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//
/*
 @property NSString *forecast_id;
 @property NSString *weather_description;
 @property NSString *weather_icon;
 @property NSString *temp;
 @property NSString *pressure;
 @property NSString *humidity;
 @property NSString *temp_min;
 @property NSString *temp_max;
 @property NSString *sea_level;
 @property NSString *grnd_level;
 @property NSString *wind_speed;
 @property NSString *wind_deg;
 @property NSString *cloud_density;
 @property NSString *rain;
 @property NSString *snow;
 @property NSDate *measure_date_time;
 @property NSDate *sunrise;
 @property NSDate *sunset;
 @property NSString *locationName;
 @property NSString *country;
 @property NSString *country_code;
 @property NSString *latitude;
 @property NSString *longitude;
 */

#import <UIKit/UIKit.h>

@interface CurrentLocationViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *weatherImage;
@property (weak, nonatomic) IBOutlet UILabel *weatherDescription;
@property (weak, nonatomic) IBOutlet UILabel *locationName;
@property (weak, nonatomic) IBOutlet UILabel *tempLbl;
@property (weak, nonatomic) IBOutlet UILabel *hrLbl;


@end
